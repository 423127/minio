## MinIO guide

** - server endpoint: minio-server.hcsdlab.com**  
** - console endpoint: minio.hcsdlab.com**

MinIO(Minimal Object Storage)는 오픈소스로 제공되는 오브젝트 스토리지 서버  
AWS S3와의 호환되는 클라우드 스토리지를 구축할 수 있는 도구, Go Lang으로 제작

- Minio Server - 클라우드 스토리지 서버(오브젝트 스토리지)를 구성
- Minio Client - "Minio Server, AWS S3, GCS 등"에 연결하여 파일 업로드 및 관리 등의 기능을 제공
- Minio Library - 개발자를 위하여 Go, Java, JavaScript, Python 등 SDK를 제공하는 라이브러리

MinIO에서 데이터 보호를 위해서 "Erasure Code" 사용 및 클러스터 구성 가능  
minio로 간편히 내부망에서 클라우드 스토리지 생성 가능하며, 대표적인 클라우드 저장소 대부분 호환 가능 S3도 거의 비슷

Client를 통해 클라우드로 대량 복사 작업을 진행할 수 있어 S3의 이전도 비교적 쉽다

---

## Run  

  
```bash
docker-compose pull
docker-compose up
```